#!/bin/bash
# ==========================================================================================================
# SCRIPT ID       :transfer_project_to_gcp.sh
# FUNCTION        :Copia de estrutura de dados project da estrutura onpremises para o GCP
# DATE ORIGINATED :2020/03/04
# AUTHOR          :MAICON SIQUEIRA | ANALISE DE PRODUCAO
# VERSION         :1.0
# CHANGE LOG	  :-
# VER           DATE            WHO                     COMMENTS
# ==========================================================================================================
# 1.0           2020/03/04      M. Siqueira             Versao Inicial
# ==========================================================================================================
#	Comando de Execução
# ==========================================================================================================
#   ./transfer_project_to_gcp.sh
# ==========================================================================================================

clear

source $PWD/lib/concurrent.lib.sh
LogDir=$PWD/log/
CONCURRENT_LOG_DIR=$LogDir
CONCURRENT_LOG_FILENAME=distcp_running_"$(date '+%Y%m')".txt


#Lista de diretorios estruturais
project_Source=(
'data/projetos/analytics/project/config'
'data/projetos/analytics/project/databases'
'data/projetos/analytics/project/jars'
'data/projetos/analytics/project/output'
'data/projetos/analytics/project/processo_diario/jars'
)

#Lista de diretorios de saidas apos processamento
project_Output=(
'data/projetos/analytics/project/processo_diario/diario_pf/output'
'data/projetos/analytics/project/processo_diario/diario_pj/output'
)

#GCP HDFS namenode address
GCP_Dest=xxx.xxx.xxx.xxx

#Onpremises HDFS namenome (set nameservice name) 
HDFS_Legacy=nameservice.cliente.corp

#HDFS root path no GCP 
GCP_HDFS_RootPath=tmp

#Função de copia: DISTCP com $1 para Source e $2 para Destination \
    # Ambiente origem é Kerberizado sendo assim, o set ipc.client.fallback-to-simple-auth-allowed=true não é funcional.
    # estratégia utilizada será NFS over HDFS to HDFS com DISTCP   

function hadoop_distcp()
{
    hadoop distcp -Ddfs.namenode.kerberos.principal.pattern=* -Dmapreduce.job.hdfs-servers.token-renewal.exclude=nanhad01p.cliente.corp,nanhad02p.cliente.corp -Dipc.client.fallback-to-simple-auth-allowed=true $1 $2

}

# Função de execução de comandos de File System no destination GCP, como: ls, rm, etc... Sendo $1 command e $2 diretorio.
    # Em caso de comandos que utilize parametros como por exemplo ls -l, inserir o comando dentro de aspas 'ls -l'

function hadoop_statement()
{
    hdfs dfs -Dipc.client.fallback-to-simple-auth-allowed=true -fs $1 $2 $3
}

# Função de checagem de dados com a data menos que 3 dias no HDFS.
    #Em caso de uso fora do HDFS substituir a expressão "hdfs dfs -ls" por "ls -ld --time-style=long-iso -t" 

function lessthan_3Days()
{
    days_diff=0
    days_top=4

    now=$(date +%s)
    hdfs dfs  -ls /$1 | while read dir_list; do
        dir_date=`echo $dir_list | awk '{print $6}'`
        filename=`echo $dir_list | awk '{print $8}'`
        difference=$(( ( $now - $(date -d "$dir_date" +%s) ) / (24 * 60 * 60 ) ))
            if [ $difference -gt $days_diff -a $difference -lt $days_top ]; then
                echo  $filename ;
            fi
    done
}

function run_hadoop_statement()
{
    for source in "${project_Source[@]}"
        do  hadoop_statement hdfs://$GCP_Dest '-mkdir -p' /$source
    done
}

# Copia de dados do HDFS ou Local path para GCP
    # Em caso de localpath incluir /* em $source/*

function run_hadoop_distcp()
{
    for source in "${project_Source[@]}"
        do hadoop_distcp  hdfs://$HDFS_Legacy/$source hdfs://$GCP_Dest/$source
    done
}
# Copia de dados do HDFS ou Local path para GCP
    # Em caso de localpath incluir /* em $source/*

function run_hadoop_distcp_lphoutput()
{
    for source in $(lessthan_3Days $project_Output)
        do hadoop_distcp hdfs://$HDFS_Legacy/$source hdfs://$GCP_Dest/$source
    done
}

function __main__()
{
    local args=(
        - "HDFS_Cria_Diretorios"        run_hadoop_statement
        - "HDFS_DistCP_Default"         run_hadoop_distcp
        - "HDFS_DistCP_Files"           run_hadoop_distcp_lphoutput
        
        --require "HDFS_Cria_Diretorios" --before-all
        --sequential
    )
    concurrent "${args[@]}"
}

__main__
